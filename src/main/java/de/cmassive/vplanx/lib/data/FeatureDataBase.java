package de.cmassive.vplanx.lib.data;

import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.Endpoint;

import java.util.ArrayList;
import java.util.List;

public abstract class FeatureDataBase {

    protected AuthContainer authContainer = new AuthContainer();

    public FeatureDataBase() { }

    public AuthContainer getAuthContainer() {
        return authContainer;
    }
    public void setAuthContainer(AuthContainer authContainer) {
        this.authContainer = authContainer;
    }
}
