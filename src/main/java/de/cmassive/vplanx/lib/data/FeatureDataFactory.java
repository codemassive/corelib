package de.cmassive.vplanx.lib.data;

import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import de.cmassive.vplanx.lib.util.reflection.NamedClassFactoryBase;
import de.cmassive.vplanx.lib.util.reflection.ReflectionUtil;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FeatureDataFactory extends NamedClassFactoryBase<FeatureDataBase> {

    private static FeatureDataFactory FACTORY = new FeatureDataFactory(FeatureDataBase.class, FeatureDataFactory.class.getPackage().getName() + ".impl");

    private FeatureDataFactory(Class<FeatureDataBase> superClass, String pkgName) {
        super(superClass, pkgName);
    }

    public static FeatureDataFactory getInstance() {
        return FACTORY;
    }

}
