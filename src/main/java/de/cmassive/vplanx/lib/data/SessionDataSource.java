package de.cmassive.vplanx.lib.data;

import de.cmassive.vplanx.lib.feature.FeatureBase;

import java.util.Set;

public interface SessionDataSource { //TODO add abstraction layer for feature data

    //FEATURE
    public Set<FeatureBase> getFeatures();
    public FeatureBase getFeature(String ftName);
    public boolean isFeaturePresent(String ftName);
    public boolean addFeature(FeatureBase ftBase);
    public boolean removeFeature(String ftName);

    public boolean reload();
}
