package de.cmassive.vplanx.lib.data;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SimpleDataObject {

    private HashMap<String, Object> objectHashMap = new HashMap<>();

    public SimpleDataObject() {

    }

    public void set(String name, Object object) {
        objectHashMap.put(name, object);
    }

    public Object get(String name) {
        return objectHashMap.get(name);
    }

    public Integer getInteger(String name) {
        return (Integer) objectHashMap.get(name);
    }

    public String getString(String name) {
        Object o = get(name);
        return o == null ? null : o.toString();
    }

    public Double getDouble(String name) {
        return (Double) objectHashMap.get(name);
    }

    public boolean containsKey(String name) {
        return objectHashMap.containsKey(name);
    }

    public Set<Map.Entry<String, Object>> entrySet() {
        return objectHashMap.entrySet();
    }

    public HashMap<String, Object> getObjectHashMap() {
        return objectHashMap;
    }

    public void setObjectHashMap(HashMap<String, Object> objectHashMap) {
        this.objectHashMap = objectHashMap;
    }
}
