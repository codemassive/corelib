package de.cmassive.vplanx.lib.data.impl;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;

@NamedClass("ft-substitution")
public class SubstitutionData extends FeatureDataBase {

    private String api;

    public SubstitutionData() {

    }

    public String getApi() {
        return api;
    }
    public void setApi(String api) {
        this.api = api;
    }
}
