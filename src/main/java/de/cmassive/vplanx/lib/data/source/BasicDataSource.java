package de.cmassive.vplanx.lib.data.source;

import de.cmassive.vplanx.lib.data.SessionDataSource;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.feature.FeatureFactory;

import java.util.HashSet;
import java.util.Set;

public class BasicDataSource implements SessionDataSource {

    private Set<FeatureBase> featureBases = new HashSet<>();

    @Override
    public Set<FeatureBase> getFeatures() {
        return featureBases;
    }

    @Override
    public FeatureBase getFeature(String ftName) {
        //for(FeatureBase base : featureBases) if(base.getName().equalsIgnoreCase(ftName)) return base;

        return null;
    }

    @Override
    public boolean isFeaturePresent(String ftName) {
        return getFeature(ftName) != null;
    }

    @Override
    public boolean addFeature(FeatureBase ftBase) {
        return featureBases.add(ftBase);
    }

    @Override
    public boolean removeFeature(String ftName) {
        return featureBases.remove(getFeature(ftName));
    }

    @Override
    public boolean reload() {
        return true;
    }

    public void addFeature(String name) {
        if(name == null || getFeature(name) != null) return;

        FeatureBase base = FeatureFactory.getInstance().newInstance(name);
        featureBases.add(base);
    }
}
