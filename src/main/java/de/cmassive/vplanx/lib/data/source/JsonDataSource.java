package de.cmassive.vplanx.lib.data.source;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.data.FeatureDataFactory;
import de.cmassive.vplanx.lib.data.SessionDataSource;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.feature.FeatureFactory;
import de.cmassive.vplanx.lib.util.format.JacksonUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class JsonDataSource implements SessionDataSource {

    private Set<FeatureBase> featureBases = new HashSet<>();
    private ObjectNode jsonData;

    public JsonDataSource(ObjectNode node) {
        this.jsonData = node;
        reload();
    }

    public JsonDataSource(String json) {
        try {
            this.jsonData = (ObjectNode) JacksonUtil.getStaticMapper().readTree(json);
            reload();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set<FeatureBase> getFeatures() {
        return this.featureBases;
    }

    @Override
    public FeatureBase getFeature(String ftName) {
        //for(FeatureBase b : this.featureBases) if(b.getName().equalsIgnoreCase(ftName)) return b;

        return null;
    }

    @Override
    public boolean isFeaturePresent(String ftName) {
        return getFeature(ftName) != null;
    }

    @Override
    public boolean addFeature(FeatureBase ftBase) {
        //if(getFeature(ftBase.getName()) != null) return false;

        this.featureBases.add(ftBase);
        return true;
    }

    @Override
    public boolean removeFeature(String ftName) {
        if(getFeature(ftName) == null) return false;

        this.featureBases.remove(getFeature(ftName));
        return true;
    }

    @Override
    public boolean reload() {
        return extractDataFromJson();
    }

    public ObjectNode getJsonData() {
        return jsonData;
    }

    public void setJsonData(ObjectNode jsonData) {
        this.jsonData = jsonData;
        extractDataFromJson();
    }

    private boolean extractDataFromJson() {
        if(jsonData == null) return false;

        if(jsonData.has("features") && jsonData.get("features").isArray()) {
            ArrayNode array = (ArrayNode) jsonData.get("features");

            for(JsonNode node : array) {
                ObjectNode obj = (ObjectNode) node;

                String ftName = obj.get("name").asText();

                FeatureBase ft = FeatureFactory.getInstance().newInstance(ftName);

                /*FeatureDataBase ftData = FeatureDataFactory.getInstance().newInstance(ftName);

                if(ftData == null || ft == null) continue;
                ftData = (FeatureDataBase) JacksonUtil.objectFromJson((ObjectNode) obj.get("data"), ftData.getClass());
                if(ftData == null) continue;

                ft.setFeatureData(ftData);*/

                this.featureBases.add(ft);
            }
        }

        return true;
    }
}
