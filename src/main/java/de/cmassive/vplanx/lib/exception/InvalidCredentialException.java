package de.cmassive.vplanx.lib.exception;

public class InvalidCredentialException extends Exception {

    public InvalidCredentialException(String message) {
        super(message);
    }
}
