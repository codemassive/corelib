package de.cmassive.vplanx.lib.feature;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.exception.InvalidCredentialException;
import de.cmassive.vplanx.lib.network.auth.Endpoint;

import java.util.ArrayList;
import java.util.List;

public abstract class FeatureBase {

    private List<Endpoint> endpoints = new ArrayList<>();

    public FeatureBase() {

    }

    public abstract boolean process(FeatureDataBase featureDataBase) throws InvalidCredentialException;

    public List<Endpoint> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(List<Endpoint> endpoints) {
        this.endpoints = endpoints;
    }
}
