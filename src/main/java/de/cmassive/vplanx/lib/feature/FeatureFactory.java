package de.cmassive.vplanx.lib.feature;

import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import de.cmassive.vplanx.lib.util.reflection.NamedClassFactoryBase;
import de.cmassive.vplanx.lib.util.reflection.ReflectionUtil;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FeatureFactory extends NamedClassFactoryBase<FeatureBase> {

    private static FeatureFactory FACTORY = new FeatureFactory();

    protected FeatureFactory() {
        super(FeatureBase.class, FeatureFactory.class.getPackage().getName() + ".impl");
    }

    public static FeatureFactory getInstance() {
        return FACTORY;
    }
}
