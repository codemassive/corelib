package de.cmassive.vplanx.lib.feature.impl;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.exception.InvalidCredentialException;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import de.cmassive.vplanx.lib.network.auth.AuthHandlerFactory;
import de.cmassive.vplanx.lib.network.auth.Endpoint;
import de.cmassive.vplanx.lib.news.News;
import de.cmassive.vplanx.lib.news.NewsParserBase;
import de.cmassive.vplanx.lib.news.NewsParserFactory;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.impl.client.BasicCookieStore;

import java.util.HashSet;
import java.util.Set;

@NamedClass("ft-news")
public class NewsFeature extends FeatureBase {

    private Set<News> newsSet = new HashSet<>();
    private String loaderName;

    private Executor executor = Executor.newInstance();
    private CookieStore store = new BasicCookieStore();

    public NewsFeature(String loaderName) {
        this.loaderName = loaderName;
    }

    @Override
    public boolean process(FeatureDataBase data) throws InvalidCredentialException {
        NewsParserBase parser = NewsParserFactory.getInstance().newInstance(getLoaderName());

        if(parser == null) {
            throw new IllegalArgumentException("Parser for api '" + getLoaderName() + "' is null!");
        }

        AuthHandler handler = null;
        if(data.getAuthContainer() != null && data.getAuthContainer().getAuthInfo() != null && data.getAuthContainer().getAuthType() != null) {
            handler = AuthHandlerFactory.getInstance().newInstance(data.getAuthContainer().getAuthType());
            if (handler == null)
                throw new IllegalArgumentException("Auth handler for type '" + data.getAuthContainer().getAuthType() + "' is null!");
        }

        for(Endpoint endpoint : getEndpoints()) {
            System.out.println("ACCESS ENDPOINT: " + endpoint.getUrl().toString());

            if (handler != null) handler.auth(executor, store, data.getAuthContainer(), endpoint.getUrl());

            try {
                Response response = executor.execute(Request.Get(endpoint.getUrl().toString()));
                if (data.getAuthContainer().getExpectedResult() != null) {
                    if (!response.returnContent().asString().toLowerCase().contains(data.getAuthContainer().getExpectedResult().toLowerCase())) {
                        continue;
                    }
                }

                this.newsSet = parser.parseNews(this, data, handler, response);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }


        return true;
    }

    public Set<News> getNewsSet() {
        return newsSet;
    }

    public String getLoaderName() {
        return loaderName;
    }
}
