package de.cmassive.vplanx.lib.feature.impl;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.exception.InvalidCredentialException;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import de.cmassive.vplanx.lib.network.auth.AuthHandlerFactory;
import de.cmassive.vplanx.lib.network.auth.Endpoint;
import de.cmassive.vplanx.lib.substitution.*;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.impl.client.BasicCookieStore;

import java.util.HashSet;
import java.util.Set;

@NamedClass("ft-substitution")
public class SubstitutionFeature extends FeatureBase {

    private SubstitutionPlan plan;
    private Set<String> classes;
    private boolean universal;

    private Executor executor = Executor.newInstance();
    private CookieStore store = new BasicCookieStore();

    private String api;

    public SubstitutionFeature(String api) {
        this.plan = new SubstitutionPlan();
        this.classes = new HashSet<>();
        this.universal = false;

        this.api = api;
    }

    @Override
    public boolean process(FeatureDataBase data) throws InvalidCredentialException {
        SubstitutionParserBase parser = SubstitutionParserFactory.getInstance().newInstance(getApi());

        if(parser == null) {
            throw new IllegalArgumentException("Parser for api '" + getApi() + "' is null!");
        }

        AuthHandler handler = null;
        if(data.getAuthContainer() != null && data.getAuthContainer().getAuthInfo() != null && data.getAuthContainer().getAuthType() != null) {
            handler = AuthHandlerFactory.getInstance().newInstance(data.getAuthContainer().getAuthType());
            if (handler == null)
                throw new IllegalArgumentException("Auth handler for type '" + data.getAuthContainer().getAuthType() + "' is null!");
        }

        for(Endpoint endpoint : getEndpoints()) {
            System.out.println("ACCESS ENDPOINT: " + endpoint.getUrl().toString());

            if(handler != null) handler.auth(executor, store, data.getAuthContainer(), endpoint.getUrl());

            try {
                Response response = executor.execute(Request.Get(endpoint.getUrl().toString()));
                if(data.getAuthContainer().getExpectedResult() != null) {
                    if(!response.returnContent().asString().toLowerCase().contains(data.getAuthContainer().getExpectedResult().toLowerCase())) {
                        continue;
                    }
                }

                //response.returnResponse();
                this.plan.addDays(parser.parseDays(this, data, handler, response));

            } catch(Exception ex) {
                ex.printStackTrace();
                /*if(ex instanceof HttpResponseException) {
                    throw new InvalidCredentialException(null);
                }*/
            }
        }

        //executor.clearAuth();
        //executor.clearCookies();
        this.classes = parser.provideClasses();
        this.universal = parser.isUniversal();

        return true;
    }

    public String getApi() {
        return api;
    }

    public SubstitutionPlan getPlan() {
        return plan;
    }

    public Set<String> getClasses() {
        return classes;
    }

    public boolean isUniversal() {
        return universal;
    }
}
