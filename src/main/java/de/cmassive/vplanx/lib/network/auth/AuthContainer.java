package de.cmassive.vplanx.lib.network.auth;

import de.cmassive.vplanx.lib.network.auth.info.AuthInfo;

public class AuthContainer {

    private AuthInfo authInfo;
    private String authType;
    private String expectedResult;

    public AuthContainer(AuthInfo info, String type) {
        this.authInfo = info;
        this.authType = type;
    }

    public AuthContainer() {

    }

    public AuthInfo getAuthInfo() {
        return authInfo;
    }

    public void setAuthInfo(AuthInfo authInfo) {
        this.authInfo = authInfo;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }
}
