package de.cmassive.vplanx.lib.network.auth;

import de.cmassive.vplanx.lib.network.auth.info.AuthInfo;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;

import java.net.URL;

public interface AuthHandler {

    public Object auth(Executor executor, CookieStore store, AuthContainer info, URL target);

}
