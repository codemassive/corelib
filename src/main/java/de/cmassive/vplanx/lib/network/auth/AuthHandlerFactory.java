package de.cmassive.vplanx.lib.network.auth;

import de.cmassive.vplanx.lib.util.reflection.NamedClassFactoryBase;

public class AuthHandlerFactory extends NamedClassFactoryBase<AuthHandler> {

    private static AuthHandlerFactory FACTORY = new AuthHandlerFactory();

    protected AuthHandlerFactory() {
        super(AuthHandler.class, AuthHandlerFactory.class.getPackage().getName());
    }

    public static AuthHandlerFactory getInstance() {
        return FACTORY;
    }
}
