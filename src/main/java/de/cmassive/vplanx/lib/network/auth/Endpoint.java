package de.cmassive.vplanx.lib.network.auth;

import java.net.URL;

public class Endpoint {

    private URL url;

    public Endpoint(URL url) {
        this.url = url;
    }

    public Endpoint() {

    }

    public void setUrl(URL url) {
        this.url = url;
    }
    public URL getUrl() {
        return url;
    }
}
