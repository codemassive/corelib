package de.cmassive.vplanx.lib.network.auth.handler;

import de.cmassive.vplanx.lib.data.SessionDataSource;
import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import de.cmassive.vplanx.lib.network.auth.info.AuthInfo;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;

import java.net.URL;

@NamedClass("auth-post")
public class FieldHandler implements AuthHandler {

    @Override
    public Object auth(Executor executor, CookieStore store, AuthContainer info, URL target) {
        return null;
    }
}
