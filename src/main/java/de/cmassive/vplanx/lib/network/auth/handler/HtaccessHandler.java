package de.cmassive.vplanx.lib.network.auth.handler;

import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import de.cmassive.vplanx.lib.network.auth.info.UserPasswordAuth;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;

import java.net.URL;

@NamedClass("auth-static")
public class HtaccessHandler implements AuthHandler {

    @Override
    public Object auth(Executor executor, CookieStore store, AuthContainer info, URL target ) {
        if(info.getAuthInfo() instanceof UserPasswordAuth) {
            UserPasswordAuth userPasswordAuth = (UserPasswordAuth) info.getAuthInfo();
            executor.auth(userPasswordAuth.getUsername(), userPasswordAuth.getPassword());
        }

        return null;
    }
}
