package de.cmassive.vplanx.lib.network.auth.info;

import java.util.HashMap;

public interface AuthInfo {

    boolean withRawData(HashMap<String, String> data);
    HashMap<String, String> toRawData();

} //MARKER INTERFACE
