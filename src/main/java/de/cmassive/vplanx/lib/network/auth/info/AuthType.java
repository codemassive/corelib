package de.cmassive.vplanx.lib.network.auth.info;

public enum AuthType {

    STATIC(UserPasswordAuth.class),
    FORM(FieldAuth.class);

    private Class<? extends AuthInfo> authClass;

    private AuthType(Class<? extends AuthInfo> authClass) {
        this.authClass = authClass;
    }

    public Class<? extends AuthInfo> getAuthClass() {
        return authClass;
    }
}
