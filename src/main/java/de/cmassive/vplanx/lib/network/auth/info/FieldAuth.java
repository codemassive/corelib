package de.cmassive.vplanx.lib.network.auth.info;

import java.util.HashMap;
import java.util.Map;

public class FieldAuth implements AuthInfo {

    private HashMap<String, String> params = new HashMap<>();

    public FieldAuth() {

    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }

    public void addParam(String k, String v) {
        params.put(k, v);
    }

    @Override
    public boolean withRawData(HashMap<String, String> data) {
        this.params = data;
        return true;
    }

    @Override
    public HashMap<String, String> toRawData() {
        return params;
    }
}
