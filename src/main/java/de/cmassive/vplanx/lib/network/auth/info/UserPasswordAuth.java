package de.cmassive.vplanx.lib.network.auth.info;

import java.util.HashMap;
import java.util.Objects;

public class UserPasswordAuth implements AuthInfo {

    private String username;
    private String password;

    public UserPasswordAuth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserPasswordAuth() {

    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPasswordAuth that = (UserPasswordAuth) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    @Override
    public boolean withRawData(HashMap<String, String> data) {
        if(!data.containsKey("username") || !data.containsKey("password")) {
            return false;
        }

        this.username = data.get("username");
        this.password = data.get("password");
        return true;
    }

    @Override
    public HashMap<String, String> toRawData() {
        HashMap<String, String> data = new HashMap<>();
        data.put("username", this.username);
        data.put("password", this.password);
        return data;
    }
}
