package de.cmassive.vplanx.lib.network.http;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.Closeable;
import java.io.IOException;

public class HttpConnection implements Closeable {

    private CloseableHttpClient client;

    public HttpConnection() {

    }

    public CloseableHttpResponse get(String urlStr) {
        try {
            HttpGet get = new HttpGet(urlStr);
            return client.execute(get);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public CloseableHttpClient getClient() {
        return this.client;
    }

    @Override
    public void close() throws IOException {
        client.close();
    }
}
