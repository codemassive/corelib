package de.cmassive.vplanx.lib.news;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.feature.impl.NewsFeature;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import org.apache.http.client.fluent.Response;

import java.util.Set;

public abstract class NewsParserBase {

    public NewsParserBase() {

    }

    public abstract Set<News> parseNews(NewsFeature feature, FeatureDataBase base, AuthHandler usedHandler, Response response);

}
