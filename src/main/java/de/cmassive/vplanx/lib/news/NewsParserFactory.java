package de.cmassive.vplanx.lib.news;

import de.cmassive.vplanx.lib.util.reflection.NamedClassFactoryBase;

public class NewsParserFactory extends NamedClassFactoryBase<NewsParserBase> {

    private static NewsParserFactory FACTORY = new NewsParserFactory(NewsParserBase.class, "de.cmassive.vplanx.lib.news.impl");

    protected NewsParserFactory(Class<NewsParserBase> superClass, String pkgName) {
        super(superClass, pkgName);
    }

    public static NewsParserFactory getInstance() {
        return FACTORY;
    }

}
