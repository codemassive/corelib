package de.cmassive.vplanx.lib.news.impl;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.feature.impl.NewsFeature;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import de.cmassive.vplanx.lib.news.News;
import de.cmassive.vplanx.lib.news.NewsParserBase;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.apache.http.client.fluent.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NamedClass("ars")
public class ARSNewsParser extends NewsParserBase {

    private Set<News> newsSet = new HashSet<>();

    @Override
    public Set<News> parseNews(NewsFeature feature, FeatureDataBase base, AuthHandler usedHandler, Response response) {
        try {
            String content = response.returnContent().asString();
            News lastTmp = null;

            Document document = Jsoup.parse(content);
            for(Element element : document.select(".contentpaneopen")) {
                List<Element> headings = element.select(".contentheading");
                if(headings.size() != 0) {
                    News n = new News();
                    n.setHeadline(headings.get(0).text());
                    newsSet.add(n);

                    lastTmp = n;
                    continue;
                }

                if(newsSet.size() > 0 && lastTmp != null) {
                    lastTmp.setContent(element.select("tr > td").first().text());

                    Element imageElement = element.select("img").first();
                    if(imageElement != null) {
                        lastTmp.setMainImageUrl("http://arwed-rossbach-schule.de/cms/" + imageElement.attr("src"));
                    }

                    lastTmp.setAuthor("ARS");
                    lastTmp.setPublishedDate(LocalDate.now());

                    lastTmp = null;
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return newsSet;
    }
}
