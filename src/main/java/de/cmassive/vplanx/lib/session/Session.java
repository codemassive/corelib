package de.cmassive.vplanx.lib.session;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.data.SessionDataSource;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.feature.FeatureFactory;
import de.cmassive.vplanx.lib.session.task.SessionTask;
import de.cmassive.vplanx.lib.util.SimpleId;
import de.cmassive.vplanx.lib.util.reflection.ReflectionUtil;

import java.util.ArrayList;
import java.util.List;

public class Session {

    private SessionTaskSource taskSource;
    private List<FeatureBase> featureBaseList = new ArrayList<>();
    private int id = SimpleId.getNextId();

    public Session() {

    }

    public void execute(SessionTask task) {
        synchronized (task) {
            task.getObserver().ifPresent((ob) -> ob.onTaskStart(task, this));

            for(FeatureBase base : featureBaseList) {
                String ftName = ReflectionUtil.getNamedClassName(base.getClass());
                if(!task.hasBucket(ftName)) continue;
                FeatureDataBase dataBase = task.getDataBucket(ftName).getFeatureData();

                try {
                    task.getObserver().ifPresent((ob) -> ob.onTaskStart(task, this));
                    base.process(dataBase);
                    task.getObserver().ifPresent((ob) -> ob.onTaskProcess(task, this, base, dataBase));
                }catch(Exception e) {
                    task.getObserver().ifPresent((ob) -> ob.onTaskError(task, this, base, dataBase, e));
                    e.printStackTrace();
                }

            }

            task.getObserver().ifPresent((ob) -> ob.onTaskFinish(task, this));
        }
    }

    public void execute(List<SessionTask> taskList) {
        for(SessionTask task : taskList) {
            execute(task);
        }
    }

    public void executeTasks() {
        if(this.taskSource != null) {
            for(SessionTask task : this.taskSource.getTasks()) {
                this.execute(task);
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SessionTaskSource getTaskSource() {
        return taskSource;
    }

    public void setTaskSource(SessionTaskSource taskSource) {
        this.taskSource = taskSource;
    }

    public void addFeature(FeatureBase base) {
        this.featureBaseList.add(base);
    }

    public void addFeature(String ftName, Object... objects) {
        this.featureBaseList.add(FeatureFactory.getInstance().newInstance(ftName, objects));
    }


    /*private static interface SessionFunctor {
        public void operation(Session.Observer observer);
    }

    public void execute(SessionDynamicData data) {
        daoObserver((observer) -> observer.onSessionStart(this));

        for(FeatureBase base : dataSource.getFeatures()) {
            if(base.isEnabled()) {
                if(base.isDynamic()) {
                    if(data == null || !data.hasData(base.getName())) continue;
                    ArrayList<FeatureDataBase> featureDataBases = data.getData(base.getName());

                    for(FeatureDataBase dataBase : featureDataBases) {
                        base.process();
                    }

                    continue;
                }

                base.process();
            }
        }

        daoObserver((observer) -> observer.onSessionFinish(this));
    }*/

    /*public FeatureDataBase getFeatureData(String ftName) {
        return dataSource.getFeature(ftName).getFeatureData();
    }

    public boolean setFeatureAuthInfo(String ftName, AuthInfo info) {
        FeatureDataBase base = getFeatureData(ftName);
        if(base == null) return false;

        base.getAuthContainer().setAuthInfo(info);
        return true;
    }

    public boolean setFeatureAuthType(String ftName, String type) {
        FeatureDataBase base = getFeatureData(ftName);
        if(base == null) return false;

        base.getAuthContainer().setAuthType(type);
        return true;
    }

    public boolean setFeatureAuthContainer(String ftName, AuthContainer container) {
        FeatureDataBase base = getFeatureData(ftName);
        if(base == null) return false;

        base.setAuthContainer(container);
        return true;
    }*/
}
