package de.cmassive.vplanx.lib.session;

import de.cmassive.vplanx.lib.session.task.SessionTask;

import java.util.List;

public interface SessionTaskSource {

    public List<SessionTask> getTasks();
    public boolean reload(int taskId);
    public boolean reload();

}
