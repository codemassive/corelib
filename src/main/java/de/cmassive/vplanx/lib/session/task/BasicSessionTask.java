package de.cmassive.vplanx.lib.session.task;

import de.cmassive.vplanx.lib.data.FeatureDataBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BasicSessionTask extends SessionTask {

    private List<TaskDataBucket> dataBuckets = new ArrayList<>();

    @Override
    public TaskDataBucket getDataBucket(String ftName) {
        for(TaskDataBucket bucket : dataBuckets) {
            if(bucket.getFeatureName().equalsIgnoreCase(ftName)) return bucket;
        }
        return null;
    }

    @Override
    public boolean hasBucket(String ftName) {
        return getDataBucket(ftName) != null;
    }

    @Override
    public List<TaskDataBucket> getDataBuckets() {
        return this.dataBuckets;
    }

    public void addData(TaskDataBucket bucket) {
        TaskDataBucket taskDataBucket = null;
        if((taskDataBucket = getDataBucket(bucket.getFeatureName())) != null) {
            taskDataBucket.setFeatureData(bucket.getFeatureData());
        } else{
            this.dataBuckets.add(bucket);
        }
    }
}
