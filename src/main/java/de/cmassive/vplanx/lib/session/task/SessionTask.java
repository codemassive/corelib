package de.cmassive.vplanx.lib.session.task;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.session.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public abstract class SessionTask {

    private Optional<Observer> observer = Optional.empty();
    private int taskId;

    public abstract TaskDataBucket getDataBucket(String ftName);
    public abstract boolean hasBucket(String ftName);
    public abstract List<TaskDataBucket> getDataBuckets();

    public Optional<Observer> getObserver() {
        return observer;
    }

    public void setObserver(Observer observer) {
        this.observer = Optional.of(observer);
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public interface Observer {

        public void onTaskStart(SessionTask task, Session session);
        public boolean onTaskProcess(SessionTask task, Session session, FeatureBase featureBase, FeatureDataBase base);
        public boolean onTaskError(SessionTask task, Session session, FeatureBase featureBase, FeatureDataBase dataBase, Exception exception);
        public void onTaskFinish(SessionTask task, Session session);

    }

}
