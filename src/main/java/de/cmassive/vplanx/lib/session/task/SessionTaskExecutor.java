package de.cmassive.vplanx.lib.session.task;

import de.cmassive.vplanx.lib.session.Session;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SessionTaskExecutor {

    private static int SERVICE_THREADS = 10;
    private static ExecutorService SERVICE = Executors.newFixedThreadPool(SERVICE_THREADS);

    public static void executeTask(Session session, SessionTask task) {
        SERVICE.execute(new TaskRunnable(session, task));
    }

    public static void executeTasks(Session session, Collection<SessionTask> tasks) {
        for(SessionTask task : tasks) {
            executeTask(session, task);
        }
    }

    public static void setServiceThreadCount(int count) {
        if(count <= 0) count = 1;

        SERVICE_THREADS = count;
        SERVICE.shutdown();
        SERVICE = Executors.newFixedThreadPool(SERVICE_THREADS);
    }

    public static ExecutorService getService() {
        return SERVICE;
    }

    private static class TaskRunnable implements Runnable {

        private Session session;
        private SessionTask sessionTask;

        public TaskRunnable(Session session, SessionTask sessionTask) {
            this.session = session;
            this.sessionTask = sessionTask;
        }

        @Override
        public void run() {
            this.session.execute(sessionTask);
        }

    }

}
