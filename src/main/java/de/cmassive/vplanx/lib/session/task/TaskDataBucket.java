package de.cmassive.vplanx.lib.session.task;

import de.cmassive.vplanx.lib.data.FeatureDataBase;

import java.util.HashMap;

public class TaskDataBucket {

    private String featureName;
    private FeatureDataBase featureData;

    public TaskDataBucket(String featureName, FeatureDataBase featureData) {
        this.featureName = featureName;
        this.featureData = featureData;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public FeatureDataBase getFeatureData() {
        return featureData;
    }

    public void setFeatureData(FeatureDataBase featureData) {
        this.featureData = featureData;
    }
}
