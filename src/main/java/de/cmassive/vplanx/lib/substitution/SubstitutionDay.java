package de.cmassive.vplanx.lib.substitution;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SubstitutionDay {

    private int id;
    private Set<Substitution> substitutionList = new HashSet<>();
    private LocalDate date;
    private String messageOfTheDay;

    public SubstitutionDay(LocalDate date) {
        this.date = date;
    }

    public void addSubstitution(Substitution substitution) {
        substitutionList.add(substitution);
    }

    public void addSubstitutions(Set<Substitution> substitutions) {
        substitutionList.addAll(substitutions);
    }

    public Set<Substitution> getSubstitutionList() {
        return substitutionList;
    }

    public void setSubstitutionList(Set<Substitution> substitutionList) {
        this.substitutionList = substitutionList;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getMessageOfTheDay() {
        return messageOfTheDay;
    }

    public void setMessageOfTheDay(String messageOfTheDay) {
        this.messageOfTheDay = messageOfTheDay;
    }

    public Substitution getSubstitution(LocalDateTime time) {
        for(Substitution sub : getSubstitutionList()) {
            if(sub.getDateTime().equals(time)) {
                return sub;
            }
        }

        return null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
