package de.cmassive.vplanx.lib.substitution;

import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.data.impl.SubstitutionData;
import de.cmassive.vplanx.lib.feature.impl.SubstitutionFeature;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import org.apache.http.client.fluent.Response;

import java.util.Set;

public abstract class SubstitutionParserBase {

    public SubstitutionParserBase() {

    }

    public abstract Set<SubstitutionDay> parseDays(SubstitutionFeature feature, FeatureDataBase base, AuthHandler usedHandler, Response response);
    public abstract Set<String> provideClasses();
    public abstract boolean isUniversal();
}
