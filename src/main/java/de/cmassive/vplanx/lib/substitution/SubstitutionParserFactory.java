package de.cmassive.vplanx.lib.substitution;

import de.cmassive.vplanx.lib.util.reflection.NamedClassFactoryBase;

public class SubstitutionParserFactory extends NamedClassFactoryBase<SubstitutionParserBase> {

    private static SubstitutionParserFactory FACTORY = new SubstitutionParserFactory(SubstitutionParserBase.class, "de.cmassive.vplanx.lib.substitution.impl");

    protected SubstitutionParserFactory(Class<SubstitutionParserBase> superClass, String pkgName) {
        super(superClass, pkgName);
    }

    public static SubstitutionParserFactory getInstance() {
        return FACTORY;
    }
}
