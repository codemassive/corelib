package de.cmassive.vplanx.lib.substitution;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SubstitutionPlan {

    private Set<SubstitutionDay> substitutionDays = new HashSet<>();

    public SubstitutionPlan() {

    }

    public void addDay(SubstitutionDay day) {
        SubstitutionDay sDay = null;
        if((sDay = getDay(day.getDate())) != null) {
            sDay.addSubstitutions(day.getSubstitutionList());
        } else {
            substitutionDays.add(day);
        }
    }

    public void addDays(Set<SubstitutionDay> days) {
        for(SubstitutionDay day : days) {
            addDay(day);
        }
    }


    public Set<SubstitutionDay> getDays() {
        return substitutionDays;
    }

    public SubstitutionDay getDay(LocalDate date) {
        for(SubstitutionDay day : substitutionDays) if(day.getDate().equals(date)) return day;

        return null;
    }

    public void setDay(LocalDate date, SubstitutionDay day) {
        this.substitutionDays.removeIf((d) -> {
           return d.getDate().equals(date);
        });

        this.substitutionDays.add(day);
    }

    public void setDays(Set<SubstitutionDay> substitutionDays) {
        this.substitutionDays = substitutionDays;
    }
}
