package de.cmassive.vplanx.lib.substitution;

import java.time.LocalDate;
import java.util.*;

public class SubstitutionUtil {

    public static Set<SubstitutionDay> mergeSubstitutions(Set<Substitution> substitutionSet) {
        Set<SubstitutionDay> days = new HashSet<>();

        HashMap<LocalDate, ArrayList<Substitution>> subList = new HashMap<>();
        for(Substitution substitution : substitutionSet) {
            LocalDate localDate = substitution.getDateTime().toLocalDate();

            if(!subList.containsKey(localDate)) {
                ArrayList<Substitution> sb = new ArrayList<>(); sb.add(substitution);
                subList.put(localDate, sb);
            } else {
                subList.get(localDate).add(substitution);
            }
        }

        for(LocalDate date : subList.keySet()) {
            SubstitutionDay day = new SubstitutionDay(date);

            for(Substitution substitution : subList.get(date)) day.addSubstitution(substitution);
            days.add(day);
        }

        return days;
    }

}
