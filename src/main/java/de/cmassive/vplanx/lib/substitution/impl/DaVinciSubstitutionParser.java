package de.cmassive.vplanx.lib.substitution.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.feature.impl.SubstitutionFeature;
import de.cmassive.vplanx.lib.network.auth.AuthHandler;
import de.cmassive.vplanx.lib.substitution.*;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.apache.http.client.fluent.Response;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

@NamedClass("davinci")
public class DaVinciSubstitutionParser extends SubstitutionParserBase {

    private Set<String> classes = new HashSet<>();

    @Override
    public Set<SubstitutionDay> parseDays(SubstitutionFeature feature, FeatureDataBase data, AuthHandler usedHandler, Response response) {
        classes.clear();

        try {
            String content = response.returnContent().asString();

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(content);
            return parseDavinciJson((ObjectNode) node);
        } catch (IOException e) {
            e.printStackTrace();
            this.classes.clear();
        }

        Set<SubstitutionDay> days = new HashSet<>();
        SubstitutionDay day = new SubstitutionDay(LocalDate.now());
        day.setMessageOfTheDay("Fehler beim Abrufen des Vertretungsplans!\nBitte kontaktiere deine Schule!");
        days.add(day);
        return days;
    }

    @Override
    public Set<String> provideClasses() {
        return classes;
    }

    @Override
    public boolean isUniversal() {
        return true;
    }


    private Set<SubstitutionDay> parseDavinciJson(ObjectNode node) {
        SubstitutionPlan plan = new SubstitutionPlan();

        Set<Substitution> unorderedSubList = new HashSet<>();

        try {
            ObjectNode result = (ObjectNode) node.get("result");
            ObjectNode schedule = (ObjectNode) result.get("displaySchedule");
            ArrayNode lessons = (ArrayNode) schedule.get("lessonTimes");

            Substitution sub = null;
            for(JsonNode jNode : lessons) {
                ObjectNode obj = (ObjectNode) jNode;

                if((sub = parseChange(obj)) != null) {
                    unorderedSubList.add(sub);
                }
            }

            //Klassen
            ArrayNode classes = (ArrayNode) result.get("classes");
            for(JsonNode jNode : classes) {
                if(jNode.get("code") == null) continue;
                this.classes.add(jNode.get("code").asText());
            }

        }catch(Exception ex) {
            ex.printStackTrace();
            return null;
        }

        return SubstitutionUtil.mergeSubstitutions(unorderedSubList);
    }

    private Substitution parseChange(ObjectNode object) {
        if((!object.has("changes") && !object.has("newSubjectCode"))) return null;
        Substitution sub = new Substitution();

        ((ArrayNode) object.get("classCodes")).forEach(o -> sub.getClasses().add(o.asText())); //Betroffene Klassen
        String davDateStr = ((ArrayNode) object.get("dates")).get(0).asText();
        String davTimeStr = object.get("startTime").asText();
        LocalDate davDate = parseDavinciDate(davDateStr);
        LocalTime davTime = parseDavinciTime(davTimeStr);
        sub.setDateTime(LocalDateTime.of(davDate, davTime));

        if(object.has("changes")) { //Änderungen
            ObjectNode changes = (ObjectNode) object.get("changes");
            if(changes.has("caption")) sub.setDescription(changes.get("caption").asText());
            if(changes.has("cancelled") && changes.get("cancelled").asText().equalsIgnoreCase("movedAway")) return sub; //Klassen haben Ausfall!
            if(changes.has("newTeacherCodes")) { ((ArrayNode) changes.get("newTeacherCodes")).forEach(o -> sub.getNewTeacher().add(o.toString()));}
        }

        if(object.has("newSubjectCode")) sub.setNewSubject(object.get("newSubjectCode").asText());

        sub.setNewRoom(((ArrayNode) object.get("roomCodes")).get(0).asText());
        if(sub.getNewSubject() == null) sub.setNewSubject(object.get("subjectCode").asText());
        ((ArrayNode) object.get("teacherCodes")).forEach(o -> sub.getNewTeacher().add(o.asText())); //TODO remove

        return sub;
    }


    private static LocalDate parseDavinciDate(String date) {
        String year = date.substring(0, 4);
        String month = date.substring(4, 6);
        String day = date.substring(6, 8);
        return LocalDate.of(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
    }

    //f.e 0800 -> 08:00 (hh:mm)
    private static LocalTime parseDavinciTime(String time) {
        String hour = time.substring(0, 2);
        String minute = time.substring(2, 4);
        return LocalTime.of(Integer.valueOf(hour), Integer.valueOf(minute));
    }
}
