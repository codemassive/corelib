package de.cmassive.vplanx.lib.util;

public class SimpleId {

    private static int ID = 0;

    public static int getNextId() {
        return ID++; //inc after return
    }

    public static int getCurrentId() {
        return ID;
    }

}
