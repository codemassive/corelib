package de.cmassive.vplanx.lib.util.date;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalField;
import java.util.Locale;

public class DateParser {

    private static String[] DATE_STR_FORMATS;
    private static String[] DATE_TIME_STR_SEPARATORS;
    private static String[] TIME_STR_FORMATS;

    public static String[] DATE_STR_FORMATTERS;
    public static DateTimeFormatter[] DATE_FORMATTERS;

    static {
        DATE_STR_FORMATS = new String[] {
                "EEEE, dd.MM.yyyy",
                "dd.MM.yyyy",
                "dd.MM",
                "d.MM",
                "dd.M",
                "ddMMyy"
        };

        DATE_TIME_STR_SEPARATORS = new String[] {
                " um ",
                ", ",
                ",",
                " "
        };

        TIME_STR_FORMATS = new String[] {
                "HH:mm",
                "HH 'Uhr'",
                "HH:mm 'Uhr'",
                "(HH:mm)",
                "(HH:mm 'Uhr')"
        };

        int lenght = DATE_STR_FORMATS.length*DATE_TIME_STR_SEPARATORS.length*TIME_STR_FORMATS.length + DATE_STR_FORMATS.length;

        System.out.println(lenght);
        DATE_STR_FORMATTERS = new String[lenght];
        DATE_FORMATTERS = new DateTimeFormatter[lenght];

        int arrayCount = 0;
        for(String fDate : DATE_STR_FORMATS) {
            DATE_FORMATTERS[arrayCount] = new DateTimeFormatterBuilder().
                    appendPattern(fDate).
                    parseDefaulting(ChronoField.YEAR, Year.now().getValue()).
                    toFormatter(Locale.GERMANY);
            DATE_STR_FORMATTERS[arrayCount] = fDate;
            arrayCount++;

            //ADD TIME
            for(String fTime : TIME_STR_FORMATS) {
                for(String fSep : DATE_TIME_STR_SEPARATORS) {
                    String fCmp = fDate + fSep + fTime;
                    DATE_FORMATTERS[arrayCount] = DateTimeFormatter.ofPattern(fCmp).withLocale(Locale.GERMANY);
                    DATE_STR_FORMATTERS[arrayCount] = fCmp;
                    arrayCount++;
                }
            }

            System.out.println("COMPLETE " + fDate);
        }

        System.out.println("FINISH AT " + arrayCount);
    }


    public static LocalDateTime parseDateTime(String dateString) {
        if(dateString == null) return null;

        LocalDateTime time = null;

        for(int i = 0; i < DATE_FORMATTERS.length; i++) {
            DateTimeFormatter formatter = DATE_FORMATTERS[i];

            try {
                if ((time = LocalDateTime.parse(dateString, formatter)) != null) {
                    if (DATE_STR_FORMATTERS[i].contains("yy")) { //if year already present
                        return time;
                    }

                    //No year present; Check
                    time = time.withYear(Year.now().getValue());
                    Duration lastYear = Duration.between(time, time.minusYears(1)).abs();
                    Duration nextYear = Duration.between(time, time.plusYears(1)).abs();
                    Duration thisYear = Duration.between(time, time.withYear(Year.now().getValue())).abs();

                    if (lastYear.compareTo(nextYear) < 0) time = time.withYear(Year.now().getValue() + 1);
                    if (thisYear.compareTo(lastYear) > 0) time = time.withYear(Year.now().getValue() - 1);

                    return time;
                }
            }catch(DateTimeParseException e) {

            }
        }

        System.out.println("RET ZERO");
        return null;
    }

    public static LocalDate parseDate(String dateString) {
        if(dateString == null) return null;

        LocalDate time = null;

        for(int i = 0; i < DATE_FORMATTERS.length; i++) {
            System.out.println("TRY: " + DATE_STR_FORMATTERS[i] + " with " + dateString);
            DateTimeFormatter formatter = DATE_FORMATTERS[i];

            try {
                if ((time = LocalDate.parse(dateString, formatter)) != null) {
                    if (DATE_STR_FORMATTERS[i].contains("yy")) { //if year already present
                        return time;
                    }

                    time = time.withYear(Year.now().getValue());
                    LocalDateTime lcDate = time.atStartOfDay();

                    //No year present; Check
                    Duration lastYear = Duration.between(lcDate, lcDate.minusYears(1)).abs();
                    Duration nextYear = Duration.between(lcDate, lcDate.plusYears(1)).abs();
                    Duration thisYear = Duration.between(lcDate, lcDate).abs();

                    if(thisYear.compareTo(lastYear) > 0) {
                        time = time.minusYears(1);
                    }

                    System.out.println("CMP " + thisYear.compareTo(lastYear));
                    System.out.println("LAST " + lastYear.toDays());
                    System.out.println("NEXT " + nextYear.toDays());
                    System.out.println("THIS " + thisYear.toDays());



                    return time;
                }
            }catch(DateTimeParseException e) {

            }
        }

        System.out.println("RET ZERO");
        return null;
    }
}
