package de.cmassive.vplanx.lib.util.format;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

public class JacksonUtil {

    private static ObjectMapper MAPPER = new ObjectMapper();

    public static <T> T objectFromJson(ObjectMapper mapper, String json, Class<T> cls) {
        try {
            return mapper.readValue(json, cls);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T> T objectFromJson(ObjectMapper mapper, ObjectNode json, Class<T> cls) {
        try {
            return mapper.readValue(json.toString(), cls);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String objectToString(ObjectMapper mapper, Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T> T objectFromJson(String json, Class<T> cls) {
        return objectFromJson(MAPPER, json, cls);
    }

    public static <T> T objectFromJson(ObjectNode json, Class<T> cls) {
        return objectFromJson(MAPPER, json, cls);
    }

    public static String objectToString(Object object) {
        return objectToString(MAPPER, object);
    }

    public static ObjectMapper getStaticMapper() {
        return MAPPER;
    }
}
