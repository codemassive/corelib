package de.cmassive.vplanx.lib.util.old;

public enum DateFlag {

    //D = day
    //M = month
    //Y = year
    /*DATE_DMY("dd.MM.yyyy"),
    DATE_DYM("dd.yyyy.MM"),
    DATE_MDY("MM.dd.yyyy"),
    DATE_MYD("MM.yyyy.dd"),
    DATE_YDM("yyyy.dd.MM"),
    DATE_YMD("yyyy.MM.dd");*/
    DATE_DMY("dd.MM.yyyy"),
    DATE_DYM("dd.yyyy.MM"),
    DATE_MDY("MM.dd.yyyy"),
    DATE_MYD("MM.yyyy.dd"),
    DATE_YDM("yyyy.dd.MM"),
    DATE_YMD("yyyy.MM.dd");

    private String defaultPattern;

    private DateFlag(String defaultPattern) {
        this.defaultPattern = defaultPattern;
    }

    public String getDefaultPattern() {
        return defaultPattern;
    }
}
