package de.cmassive.vplanx.lib.util.old;

import java.util.ArrayList;
import java.util.List;

public class DateFlags {

    private List<DateFlag> flags = new ArrayList<>();

    public DateFlags() {

    }

    public DateFlags(List<DateFlag> flags) {
        this.flags = flags;
    }

    public List<DateFlag> getFlags() {
        return flags;
    }

    public void setFlags(List<DateFlag> flags) {
        this.flags = flags;
    }

    public void addFlag(DateFlag flag) {
        this.flags.add(flag);
    }

    public boolean hasFlag(DateFlag flag) {
        return flags.contains(flag);
    }

    public boolean hasOneOf(DateFlag... flags) {
        for(DateFlag flag : flags) {
            if(this.flags.contains(flag)) return true;
        }

        return false;
    }

    public DateFlag getDateCmpFlag(DateFlag dft) {
        for(DateFlag flag : flags) if(flag.toString().startsWith("DATE_")) return flag;

        return dft;
    }
}
