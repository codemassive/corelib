package de.cmassive.vplanx.lib.util.old;

import java.time.temporal.TemporalAccessor;
import java.util.regex.Matcher;

public interface DateInterface {

    public TemporalAccessor operation(Matcher matcher, String m, DateFlags flags);

}
