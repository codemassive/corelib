package de.cmassive.vplanx.lib.util.old;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateParser {

    public static Map<Pattern, DateInterface> PATTERN = new HashMap<>();

    private static String[] DATE_STR_FORMATS;
    private static String[] TIME_STR_FORMATS;
    private static String[] DATE_TIME_STR_SEPARATORS;
    private static DateTimeFormatter[] MIXED_FORMATS;

    static {
        PATTERN.put(Pattern.compile("(\\d{1,4})(\\D)(\\d{1,4})(\\D)(\\d{1,4})"), (matcher, m, flags) -> {
            DateFlag pFlag = DateFlag.DATE_DMY;
            String terminator = ".";

            for(DateFlag f : flags.getFlags()) if(f.toString().startsWith("DATE_")) pFlag = f;
            String pattern = pFlag.getDefaultPattern();

            if(matcher.group(2) != null || matcher.group(4) != null) {
                if(matcher.group(4) != null) terminator = matcher.group(4);
                else terminator = matcher.group(2);
            }

            if(!terminator.equalsIgnoreCase(".")) pattern = pattern.replace(".", terminator);

            return LocalDate.parse(m, DateTimeFormatter.ofPattern(pattern));
        });

        PATTERN.put(Pattern.compile("\\d{6}"), ((matcher, m, flags) -> {
            return LocalDate.parse(m, DateTimeFormatter.ofPattern(flags.getDateCmpFlag(DateFlag.DATE_DMY).getDefaultPattern()));
        }));

        DATE_STR_FORMATS = new String[] {
                //FIRST PART OF LIST FROM https://www.vsni.co.uk/software/genstat/htmlhelp/spread/DateFormats.htm
                "dd/mm/yy",
                "dd/mm/yyyy",
                "d/m/yy",
                "dd/mm/yy",
                "d/mm/yyyy",
                "d/m/yy",
                "d/m/yyyy",
                "dd.mm.yyyy",
        };

        TIME_STR_FORMATS = new String[] {
                "HH:mm",
                "()"
        };
    }

    public static TemporalAccessor parseDate(String dateString, DateFlags flags) {
        for(Pattern p : PATTERN.keySet()) {
            Matcher m = p.matcher(dateString);
            if(m.find()) {
                return PATTERN.get(p).operation(m, dateString, flags);
            }
        }

        System.out.println("NO MATCH");
        return null;
    }

    public static TemporalAccessor parseDate(String dateString, List<DateFlag> flags) {
        return parseDate(dateString, new DateFlags(flags));
    }

    public static TemporalAccessor parseDate(String dateString, DateFlag... flags) {
        return parseDate(dateString, new DateFlags(Arrays.asList(flags)));
    }

}
