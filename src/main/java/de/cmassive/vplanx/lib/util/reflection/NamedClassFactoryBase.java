package de.cmassive.vplanx.lib.util.reflection;

import java.util.HashMap;

public abstract class NamedClassFactoryBase<T> {

    private HashMap<String, Class<? extends T>> classes = new HashMap<>();
    private Class<T> superClass;

    protected NamedClassFactoryBase(Class<T> superClass, String pkgName) {
        this.superClass = superClass;
        queryClasses(pkgName);
    }

    public void overrideClass(String str, Class<? extends T> cls) {
        classes.put(str, cls);
    }

    public T newInstance(String str, Object... objects) {
        if(!classes.containsKey(str)) return null;

        return ReflectionUtil.constructObject(classes.get(str), objects);
    }

    public boolean isConstructable(String str, Object... objects) {
        try {
            Object crt = newInstance(str, objects);
            return crt != null;
        } catch(Exception ex) {
            return false;
        }
    }

    public Class<? extends T> classOf(String str) {
        return classes.get(str);
    }

    public NamedClassFactoryBase<T> addPackage(String pkgName) {
        queryClasses(pkgName);
        return this;
    }

    public HashMap<String, Class<? extends T>> getClasses() {
        return classes;
    }

    public Class<T> getSuperClass() {
        return superClass;
    }

    protected void queryClasses(String pkg) {
        classes.putAll(ReflectionUtil.searchNamedClasses(pkg, superClass));
    }

}
