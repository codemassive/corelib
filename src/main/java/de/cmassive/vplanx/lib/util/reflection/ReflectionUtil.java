package de.cmassive.vplanx.lib.util.reflection;

import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ReflectionUtil {

    public static <T> T constructObject(Class<T> cls, Class[] paramTypes, Object[] objects) {
        if(cls == null) return null;
        Constructor[] constructors = cls.getDeclaredConstructors();
        Constructor selected = null;

        outer:
        for(Constructor c : constructors) {
            if(c.getParameterCount() == paramTypes.length) {
                inner:
                for(int i = 0; i < c.getParameterTypes().length; i++) {
                    if(!c.getParameterTypes()[i].equals(paramTypes[i])) {
                        //System.out.println(c.getParameterTypes()[i].getName() + " NOT " + paramTypes[i].getName());
                        continue outer;
                    }
                }
                selected = c;
            }
        }
        if(selected == null) {
            return null;
        }
        selected.setAccessible(true);
        try {
            return (T) selected.newInstance(objects);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T constructObject(Class<T> cls) {
        return constructObject(cls, new Class[]{}, new Object[]{});
    }

    public static <T> T constructObject(Class<T> cls, Object[] objects) {
        Class[] types = new Class[objects.length];
        for(int i = 0; i < objects.length; i++) types[i] = objects[i].getClass();

        return constructObject(cls, types, objects);
    }

    public static <T> HashMap<String, Class<? extends T>> searchNamedClasses(String pkgName, Class<T> mustBeSuperclass) {
        Reflections reflections = new Reflections(pkgName);
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(NamedClass.class);

        HashMap<String, Class<? extends T>> map = new HashMap<>();

        for(Class cls : classes) {
            if(mustBeSuperclass.isAssignableFrom(cls)) {
                map.put(((NamedClass) cls.getDeclaredAnnotation(NamedClass.class)).value() , cls);
            }
        }
        return map;
    }

    public static HashMap<String, Class<?>> searchNamedClasses(String pkgName) {
        return searchNamedClasses(pkgName, Object.class);
    }

    public static String getNamedClassName(Class<?> cls) {
        if(cls.isAnnotationPresent(NamedClass.class)) {
            NamedClass namedClass = cls.getDeclaredAnnotation(NamedClass.class);
            return namedClass.value();
        }
        return null;
    }

}
