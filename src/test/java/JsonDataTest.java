import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.cmassive.vplanx.lib.data.source.JsonDataSource;
import de.cmassive.vplanx.lib.data.impl.SubstitutionData;
import de.cmassive.vplanx.lib.feature.impl.SubstitutionFeature;
import de.cmassive.vplanx.lib.network.auth.info.UserPasswordAuth;
import de.cmassive.vplanx.lib.session.Session;
import de.cmassive.vplanx.lib.substitution.Substitution;
import de.cmassive.vplanx.lib.substitution.SubstitutionDay;
import org.junit.Test;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

public class JsonDataTest {

    @Test
    public void testJsonData() {
        /*String json =
                        "{" +
                        "\"features\" : [" +
                            "{" +
                                "\"name\" : \"ft-substitution\"," +
                                "\"data\" : {" +
                                    "\"api\": \"davinci\"," +
                                    "\"endpoints\" : [ { \"url\" : \"http://localhost/file.json\" }, { \"url\" : \"http://localhost/file.json\" } ]" +
                                "}" +
                                "}" +
                             "]" +
                        "}";

        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonDataSource source = new JsonDataSource((ObjectNode) mapper.readTree(json));
            Session session = new Session(source);

            SubstitutionData data = new SubstitutionData();
            System.out.println(data.getApi());
            System.out.println(data.getEndpoints().get(0).getUrl().toString());

            //session.setFeatureAuthInfo("ft-substitution", new UserPasswordAuth("schueler", "ars2017"));
            //data.setAuthType("auth-static");

            session.getDataSource().getFeature("ft-substitution").process();
            SubstitutionFeature ft = (SubstitutionFeature) session.getDataSource().getFeature("ft-substitution");

            System.out.println("ANZAHL AN TAGEN: " + ft.getPlan().getDays().size());

            for(SubstitutionDay day : ft.getPlan().getDays()) {
                System.out.println(day.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                for(Substitution s : day.getSubstitutionList()) {
                    System.out.println(s.getDateTime().format(DateTimeFormatter.ofPattern("HH:mm")) + "  " + s.getDescription());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

}
