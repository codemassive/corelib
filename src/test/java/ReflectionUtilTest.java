import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.util.reflection.ReflectionUtil;
import org.junit.Test;

import java.util.HashMap;

public class ReflectionUtilTest {

    @Test
    public void testNamedClass() {
        HashMap<String, Class<? extends FeatureDataBase>> map = ReflectionUtil.searchNamedClasses("de.cmassive.vplanx.lib.data.impl", FeatureDataBase.class);
        for(String s : map.keySet()) {
            System.out.println(s + " - " + map.get(s).getName());
        }
    }

}
